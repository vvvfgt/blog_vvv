<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->truncate();

        for ($i=1;$i<=3;$i++) {
            DB::table('comments')->insert([
                [
                    'post_id' => $i,
                    'text' => 'Комментарий  '.$i.'/'.$i.'/0',
                    'parent_id' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            ]);
        }

       // Добавляем 10 комментариев 1 уровня к постам
       for ($i=1;$i<=10;$i++) {
           $post_id = rand(1,3);
           DB::table('comments')->insert([
               [
                   'post_id' => $post_id,
                   'text' => 'Комментарий '.$post_id.'/'.($i+3).'/0',
                   'parent_id' => 0,
                   'created_at' => Carbon::now(),
                   'updated_at' => Carbon::now(),
               ],
              ]);
       }

       // Получить доп комментарии в количестве 40 шт
       $col_rec = 12;
       for ($i=1;$i<=40;$i++) {
          $parent_id = rand(1,$col_rec+$i);
           $post = DB::table('comments')->select('post_id')->find($i);
           DB::table('comments')->insert([
               [
                   'post_id' => $post->post_id,
                   'text' => 'Комментарий '.$post->post_id .'/'.($i+13).'/'. $parent_id,
                   'parent_id' => $parent_id,
                   'created_at' => Carbon::now(),
                   'updated_at' => Carbon::now(),
               ],
           ]);
       }
    }
}
