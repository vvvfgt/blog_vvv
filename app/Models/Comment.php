<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['post_id','text','parent_id'];

    public function post()
    {
       return $this->belongsTo(Post::class);
    }


    public static function ret_list($id)
    {

       $list = $id;

       $comments = Comment::where('parent_id',$id)->get();

        foreach ($comments as $comment) {

           $list .= ','.$comment->id;

           $ret = self::ret_list($comment->id);

           if ($ret != '') $list .= ','.$ret;
        }

        return $list;

    }

    public static function array_comments($id)
    {

       $mas = [];

        $comments = Comment::where('parent_id',$id)->get();

        foreach ($comments as $comment) {
           $mas[] = array(
               'text' => $comment->text,
               'id' => $comment->id,
               'post_id' => $comment->post_id,
               'parent_id' => $comment->parent_id,
               'comments' => self::array_comments($comment->id)
           );


        }

        return $mas;
    }

    public static function array_comments_post($post_id)
    {
        $mas = [];

        $comments = Comment::where([
                               ['post_id',$post_id],
                               ['parent_id',0]
         ])->get();

        foreach ($comments as $comment ) {
            $mas[] = array(
                'text' => $comment->text,
                'id' => $comment->id,
                'post_id' => $comment->post_id,
                'parent_id' => $comment->parent_id,
                'comments' => self::array_comments($comment->id)
            );
        }

        return $mas;
    }

}
