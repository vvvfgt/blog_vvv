<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Post;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function comments($post_id)
    {

       return CommentResource::collection(Comment::where('post_id',$post_id)->get());

    }

// Добавить комментарий к посту
 public function commentPost(Request $request)
{

    $rule = [
       'text' => 'required',
       'post_id' => 'required'
    ];

    $validator = Validator::make($request->all(),$rule);

    if ($validator->fails()) {
       return response()->json($validator->errors(),400);
    }

    $post = Post::find($request->post_id);

    if (is_null($post)) {
        return response()->json(['error' => true,'message' => 'Not Found Post'],404);
    }

    $comment = Comment::create($request->all());
    return response()->json($comment,201);
}

    public function commentComment (Request $request)
    {
        $rule = [
            'text' => 'required',
            'id' => 'required'
        ];

        $validator = Validator::make($request->all(),$rule);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $comment = Comment::find($request->id);
        if (is_null($comment)) {
            return response()->json(['error' => true,'message' => 'Not Found Comment'],404);
        }

        $comment = Comment::create([
            'post_id' => $comment->post_id,
            'text' => $request->text,
            'parent_id' => $request->id,
        ]);

        return response()->json($comment,201);

    }

    public function commentEdit(Request $request, $id)
    {

       $rule = [
           'text' => 'required',
       ];

        $validator = Validator::make($request->all(),$rule);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

       $comment = Comment::find($id);
        if (is_null($comment)) {
            return response()->json(['error' => true,'message' => 'Not Found'],404);
        }

       $comment->update($request->all());

       return response()->json($comment,200);

    }

    public function commentDelete($id)
    {
        $comment = Comment::find($id);

        if (is_null($comment)) {
            return response()->json(['error' => true,'message' => 'Not Found'],404);
        }

        // Получаем массив id для удаления
        $mas = explode(',',Comment::ret_list($id));

        Comment::whereIn('id',$mas)->delete();

        return response()->json('',204);

    }

    public function comments_all($id)
    {

        $comment = Comment::find($id);

        if (is_null($comment)) {
            return response()->json(['error' => true,'message' => 'Not Found'],404);
        }

        $mas = Comment::array_comments($id);

       return response()->json($mas,200);

    }

    public function comments_post_all($post_id)
    {

       $post = Post::find($post_id);

        if (is_null($post)) {
            return response()->json(['error' => true,'message' => 'Not Found'],404);
        }

       $mas = Comment::array_comments_post($post_id);

       return response()->json($mas,200);

    }
}
