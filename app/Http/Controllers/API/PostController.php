<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PostController extends Controller
{
    public function posts()
    {
       return PostResource::collection(Post::all());
    }

    public function postById($id)
    {
        $post = Post::findOrFail($id);

        return new PostResource($post);
    }

    public function postSave(PostRequest $request)
    {

       $post = Post::create($request->validated());
       return new PostResource($post);
    }

    public function postEdit(PostRequest $request,Post $post)
    {
       $post->update($request->validated());
       return new PostResource($post);
    }

    public function postDelete(Post $post)
    {
       $post->comments()->delete();
       $post->delete();
       return response()->json('',204);
    }
}
