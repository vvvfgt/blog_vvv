<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAllCommentsPost()
    {
        $response = $this->get('/api/comments/1');

        $response->assertStatus(200);
    }

    public function testCommentPost()
{
    $response = $this->post('/api/comment_post',[
        'post_id' =>2,
        'text' => 'Комментарий к посту',
    ]);

    $this->assertEquals('201',$response->status());
}

    public function testCommentNoPost()
    {
        $response = $this->post('/api/comment_post',[
            'post_id' =>121,
            'text' => 'Комментарий к посту',
        ]);

        $this->assertEquals('404',$response->status());
    }

    public function testCommentNoInf()
    {
        $response = $this->post('/api/comment_post',[
            'text' => 'Комментарий к посту',
        ]);

        $this->assertEquals('400',$response->status());
    }

    public function testCommentComment()
    {
        $response = $this->post('/api/comment_comment',[
            'id' =>2,
            'text' => 'Комментарий к посту',
        ]);

        $this->assertEquals('201',$response->status());
    }

    public function testCommentNoComment()
    {
        $response = $this->post('/api/comment_comment',[
            'id' =>121,
            'text' => 'Комментарий к посту',
        ]);

        $this->assertEquals('404',$response->status());
    }

    public function testCommentEdit()
    {
        $response = $this->put('/api/comment/2',[
            'text' => 'Комментарий изменил',
        ]);

        $response->assertStatus(200);
    }

    public function testCommentNoEdit()
    {
        $response = $this->put('/api/comment/2');

        $this->assertEquals('400',$response->status());
    }

    public function testCommentDelete()
    {
        $response = $this->delete('/api/comment/3');

        $this->assertEquals('204', $response->status());
    }


}
