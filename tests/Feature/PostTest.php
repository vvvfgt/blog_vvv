<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPosts()
    {
        $response = $this->get('/api/posts');
        $response->assertOk();
    }

    public function testNoFond()
    {
        $response = $this->get('/api/postss');
        $this->assertEquals(404,$response->status());
    }

    public function testPost()
    {
        $response = $this->get('/api/post/1');
        $response->assertOk();
    }

    public function testStorePost()
    {
        $response = $this->post('/api/post',[
            'title' => 'Новый пост',
        ]);
        $this->assertEquals('201',$response->status());
    }

    public function testUnProcessablePost()
    {
        $response = $this->post('/api/post');
        $this->assertEquals('302',$response->status());
    }

    public function testEditPost()
    {
      $response = $this->put('/api/post/1',[
         'title' => 'Изменил',
      ]);
      $response->assertOk();
    }

    public function testEditNoPost()
    {
        $response = $this->put('/api/post/101', [
            'title' => 'Изменил',
        ]);

        $this->assertEquals('404', $response->status());
    }

    public function testDeletePost()
    {
        $response = $this->delete('/api/post/11');

        $this->assertEquals('204', $response->status());
    }
}
