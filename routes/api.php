<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('posts',[\App\Http\Controllers\API\PostController::class,'posts']);
Route::get('post/{id}',[\App\Http\Controllers\API\PostController::class,'postById']);

Route::post('post',[\App\Http\Controllers\API\PostController::class,'postSave']);
Route::put('post/{post}',[\App\Http\Controllers\API\PostController::class,'postEdit']);
Route::delete('post/{post}',[\App\Http\Controllers\API\PostController::class,'postDelete']);

Route::get('comments/{post_id}',[\App\Http\Controllers\API\CommentController::class,'comments']);
Route::post('comment_post',[\App\Http\Controllers\API\CommentController::class,'commentPost']);
Route::post('comment_comment',[\App\Http\Controllers\API\CommentController::class,'commentComment']);
Route::put('comment/{id}',[\App\Http\Controllers\API\CommentController::class,'commentEdit']);
Route::delete('comment/{id}',[\App\Http\Controllers\API\CommentController::class,'commentDelete']);

Route::get('comments_all/{id}',[\App\Http\Controllers\API\CommentController::class,'comments_all']);
Route::get('comments_post/{post_id}',[\App\Http\Controllers\API\CommentController::class,'comments_post_all']);
